let video, poseNet, pose, skeleton;

let ready = false;

function setup() {
  createCanvas(640, 480);
  video = createCapture(VIDEO);
  video.hide();
  poseNet = ml5.poseNet(video, modelLoaded);
  poseNet.on("pose", gotPoses);
}

function gotPoses(poses) {
  //console.log(poses);
  if (poses.length > 0) {
    pose = poses[0].pose;
    skeleton = poses[0].skeleton;
  }
}

function modelLoaded() {
  ready = true;
}

function draw() {
  if (ready) {
    image(video, 0, 0);

    if (pose) {
      for (let i = 5; i < pose.keypoints.length; i++) {
        let x = pose.keypoints[i].position.x;
        let y = pose.keypoints[i].position.y;
        fill(0, 255, 0);
        ellipse(x, y, 16, 16);
        textSize(32);
        getAngle(i, {
          x: x,
          y: y,
        });
        // if (angle) {
        //   text(angle, x, y);
        // }
      }

      // for (let i = 0; i < skeleton.length; i++) {
      //   let a = skeleton[i][0];
      //   let b = skeleton[i][1];
      //   strokeWeight(2);
      //   stroke(255);
      //   line(a.position.x, a.position.y, b.position.x, b.position.y);
      // }
    }
  }
}

const getAngle = (i, B) => {
  const A = getPrev(i);
  const C = getNext(i);

  if (A && B && C) {
    const AB = Math.sqrt(Math.pow(B.x - A.x, 2) + Math.pow(B.y - A.y, 2));
    const BC = Math.sqrt(Math.pow(B.x - C.x, 2) + Math.pow(B.y - C.y, 2));
    const AC = Math.sqrt(Math.pow(C.x - A.x, 2) + Math.pow(C.y - A.y, 2));
    const angle = Math.round(
      (Math.acos((BC * BC + AB * AB - AC * AC) / (2 * BC * AB)) * 180) / Math.PI
    );

    strokeWeight(2);
    stroke(255);
    line(A.x, A.y, B.x, B.y);
    line(B.x, B.y, C.x, C.y);
    let a1 = getOnLine(B.x, B.y, A.x, A.y, 0.1);
    let b1 = getOnLine(B.x, B.y, C.x, C.y, 0.1);
    noFill();
    stroke(240, 0, 0);
    curve(B.x, B.y, a1.x, a1.y, b1.x, b1.y, B.x, B.y);

    fill(0, 0, 0);
    stroke(255, 255, 255);
    text(angle, B.x, B.y);
  }
};

function getOnLine(x1, y1, x2, y2, percentage) {
  return {
    x: x1 * (1.0 - percentage) + x2 * percentage,
    y: y1 * (1.0 - percentage) + y2 * percentage,
  };
}

//TODO: By doing this for 2 people, the difference between the 2 can be calculated, and so the correctness of the moves can be determiend.
getPrev = position => {
  let i;
  switch (position) {
    case 5:
      i = 6;
      break;
    case 6:
      i = 5;
      break;
    case 7:
      i = 5;
      break;
    case 8:
      i = 6;
      break;
    default:
      break;
  }
  return i
    ? {
        x: pose.keypoints[i].position.x,
        y: pose.keypoints[i].position.y,
      }
    : false;
};

getNext = position => {
  let i;
  switch (position) {
    case 5:
      i = 7;
      break;
    case 6:
      i = 8;
      break;
    case 7:
      i = 9;
      break;
    case 8:
      i = 10;
      break;
    default:
      break;
  }
  return i
    ? {
        x: pose.keypoints[i].position.x,
        y: pose.keypoints[i].position.y,
      }
    : false;
};
